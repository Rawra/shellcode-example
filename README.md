# Shellcode-example

## Description

This project was done as a learning excercise and aims to showcase how to print a MessageBoxA without directly linking with WinAPI related libs.
Use-cases would either be reduced file size or shellcode.

It does this by only relying on the application's GS register which holds the TEB.
With the TEB we can gradually work downwards into the LDR which can tell us what modules are loaded (and on windows, there are always neccessities loaded)
Such as ntdll and kernel32 + kernelbase. Using kernel32 we can use its address to work down its DOS HEADER structure until
we get to the Export Address Table (EAT), from there on we traverse it (respecting the VA/RVAs) until we find GetProcAddress and use that to
load everything else we might need.

## Installation

Clone and compile with MSVC in RELEASE mode.
Don't turn on any optimizations on the Shellcode library itself.

## Usage

- Write your code as you would in the ShellcodeLib library, make sure to not rely on linking or anything else fancy.
- Once finished, compile it and open it up with CFF Explorer and go to Section Headers, look at the .text section's Raw Address.
- Open up your favourite HexEditor and go to the raw address.
- Proceed to copy the opcodes (ImHex can do this nicely in C++ style) from there on out until you reach the end of your program's compiled shellcode.
- Put the shellcode bytes into some array, mark it as executeable and go ham. 

## References

https://learn.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-image_data_directory
https://upload.wikimedia.org/wikipedia/commons/1/1b/Portable_Executable_32_bit_Structure_in_SVG_fixed.svg
https://www.codeproject.com/Articles/5304605/Creating-Shellcode-from-any-Code-Using-Visual-Stud
https://cocomelonc.github.io/tutorial/2021/10/27/windows-shellcoding-1.html
https://cocomelonc.github.io/tutorial/2021/10/30/windows-shellcoding-2.html
https://stackoverflow.com/questions/20507003/how-to-find-exported-functions-addresses-in-64bit-dlls