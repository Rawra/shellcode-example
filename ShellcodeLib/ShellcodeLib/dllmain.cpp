#pragma comment(linker, "/merge:.rdata=.text")

/// <summary>
/// Very basic implementation of strlen
/// </summary>
/// <param name="x"></param>
/// <returns></returns>
static unsigned long long strlen_raw(const char* x) {
	unsigned long long i = 0;
	while (true) {
		if (x[i] == '\0')
			break;
		i++;
	}
	return i;
}

/// <summary>
/// Very basic strcmp-akin function (does not give same results though)
/// </summary>
/// <param name="x"></param>
/// <param name="y"></param>
/// <returns></returns>
static int strcmp_raw(const char* x, const char* y) {
	unsigned long long len_x = strlen_raw(x);
	unsigned long long len_y = strlen_raw(y);
	if (len_x != len_y)
		return -1;

	for (int i = 0; i < len_x; i++) {
		if (x[i] != y[i])
			return -1;
	}
	return 0;
}

/// <summary>
/// Get the TEB of the current process (part of the TIB)
/// Reference: https://en.wikipedia.org/wiki/Win32_Thread_Information_Block
/// </summary>
/// <returns></returns>
static unsigned long long get_teb() {
	unsigned long long teb_address;
	asm("movq %%gs:0x30, %0" : "=r" (teb_address)); // Linear address of Thread Environment Block (TEB)
	return teb_address;
}

/// <summary>
/// Get the PEB of the current process (part of the TIB)
/// Reference: https://learn.microsoft.com/en-us/windows/win32/api/winternl/ns-winternl-peb
/// </summary>
/// <returns></returns>
static unsigned long long get_peb() {
	unsigned long long peb_address;
	asm("movq %%gs:0x60, %0" : "=r" (peb_address)); // Linear address of Process Environment Block (PEB) 
	return peb_address;
}

/// <summary>
/// Check if the process is being debugged via means of PEB flag
/// </summary>
/// <param name="peb"></param>
/// <returns></returns>
static bool is_being_debugged(unsigned long long peb) {
	char value = *((char*)(peb + 0x02));
	return (bool)value;
}

/// <summary>
/// Get the ImageBase from the PEB
/// </summary>
/// <param name="peb"></param>
/// <returns></returns>
static unsigned long long get_peb_imagebase(unsigned long long peb) {
	unsigned long long imagebase = *((unsigned long long*)(peb + 0x10));
	return imagebase;
}

/// <summary>
/// Get the LDR from the PEB
/// Reference: https://learn.microsoft.com/en-us/windows/win32/api/winternl/ns-winternl-peb_ldr_data
/// </summary>
/// <param name="peb"></param>
/// <returns></returns>
static unsigned long long get_peb_ldr(unsigned long long peb) {
	unsigned long long ldr_address = *((unsigned long long*)(peb + 0x018));
	return ldr_address;
}

/// <summary>
/// Load the Kernel32 HModule (Dll Base Address) via PEB/LDR
/// Reference:  
/// https://learn.microsoft.com/en-us/windows/win32/api/winternl/ns-winternl-peb_ldr_data
/// LIST_ENTRY: https://www.codeproject.com/Articles/800404/Understanding-LIST-ENTRY-Lists-and-Its-Importance (See graphic)
/// Note: Every Flink and Blink points to _LDR_DATA_TABLE_ENTRY.InMemoryOrderLinks and not the start of the struct.
/// </summary>
/// <param name="ldr"></param>
/// <returns></returns>
static unsigned long long get_kernel32_hmodule(unsigned long long ldr) {
	// listEntry = &ldr.InMemoryOrderModuleList
	unsigned long long listEntry = ldr + 0x20;

	// nextEntry = listEntry.Flink
	unsigned long long nextEntry = *((unsigned long long*)(listEntry + 0x00));

	// When iterating here, there is a constant set of results in order we'll get:
	// OurApp.exe DllBase       0
	// ntdll.dll DllBase        1
	// KERNEL32.DLL DllBase     2
	// KERNELBASE.dll DllBase   3   (Essentials now end)
	// ucrtbase.dll DllBase     4
	// VCRUNTIME140.dll DllBase 5
	int i = 0;
	while (nextEntry != listEntry) {
		// dataEntry = PLDR_DATA_TABLE_ENTRY
		unsigned long long dataEntry = nextEntry - 0x10; // -0x10 due to structure of _LDR_DATA_TABLE_ENTRY (See WinDbg 'dt nt!_LDR_DATA_TABLE_ENTRY')

		//if (i == 1) {
		if (i == 2) {
			// We "should" be dealing with Kernel32
			// dataEntry.DllBase
			return *((unsigned long long*)(dataEntry + 0x30));
		}
		i++;

		nextEntry = *((unsigned long long*)(nextEntry + 0x00));
	}
	return 0;
}

/// <summary>
/// This is basically our very own GetProcAddress implementation, we only need it once though to get the systems GetProcAddress instead.
/// Reference:
/// https://learn.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-image_nt_headers64
/// https://learn.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-image_file_header
/// https://learn.microsoft.com/bs-latn-ba/windows/win32/api/winnt/ns-winnt-image_data_directory
/// </summary>
/// <param name="kernel32_base"></param>
/// <returns></returns>
static unsigned long long get_getprocaddress_address(unsigned long long kernel32_base) {
	// kernel32_base: Begins with "MZ" header
	// kernel32_base = _IMAGE_DOS_HEADER
	// image_file_header = _IMAGE_DOS_HEADER.e_lfanew (new exe file header)
	// image_file_header = IMAGE_NT_HEADERS64
	int e_lfanew_offset = *((int*)(kernel32_base + 0x03C));                     // This offset needs to be applied to start of _IMAGE_DOS_HEADER (aka base)
	unsigned long long image_nt_headers64 = kernel32_base + e_lfanew_offset;    // This part starts with "PE"

	// Check the sections amount
	unsigned short num_sections = *((unsigned short*)(image_nt_headers64 + 0x04 + 0x02));

	// Parse the Export Directory (luckily its the first data directory)
	unsigned long long dd_exportdirectory = image_nt_headers64 + 0x18 + 0x70;

	// Offsets are relative to the beginning of the optional header.
	int section_offset = *((unsigned long long*)(dd_exportdirectory + 0x00));
	int section_size = *((unsigned long long*)(dd_exportdirectory + 0x04));

	// eat = _IMAGE_EXPORT_DIRECTORY
	unsigned long long image_base = *((unsigned long long*)(image_nt_headers64 + 0x18 + 0x18)); // The image base here is equivalent to kerne32_base
	unsigned long long eat = image_base + section_offset;
	unsigned int number_of_functions = *((unsigned int*)(eat + 0x14));
	unsigned int number_of_names = *((unsigned int*)(eat + 0x18));
	unsigned int address_of_functions = *((unsigned int*)(eat + 0x1C));
	unsigned int address_of_names = *((unsigned int*)(eat + 0x20));

	unsigned long long function_table = image_base + address_of_functions;
	unsigned long long name_table = image_base + address_of_names;
	/*
		You can envision the name table as an array-like structure where each element corresponds to the name of an exported function.
		However, unlike a traditional array, the elements are not contiguous blocks of memory but rather RVAs pointing to the actual names
		stored elsewhere in memory.
	*/
	unsigned long long get_proc_address = 0;
	for (int i = 0; i < number_of_names; i++) {
		const char* function_name = (const char*)(image_base + ((unsigned int*)name_table)[i]);

		if (strcmp_raw(function_name, "GetProcAddress") == 0) {
			get_proc_address = image_base + ((unsigned int*)function_table)[i];
			break;
		}
	}

	return get_proc_address;
}

// https://blackcloud.me/Win32-shellcode-3/
// https://cocomelonc.github.io/tutorial/2021/10/30/windows-shellcoding-2.html
// https://www.codeproject.com/Articles/5304605/Creating-Shellcode-from-any-Code-Using-Visual-Stud
// Shellcode purpose: MessageBoxA
extern "C" unsigned long _code() {
	// Retrieve our TEB->PEB->LDR
	//unsigned long long TEB = get_teb();
	unsigned long long PEB = get_peb();
	//unsigned long long PEB_IMAGEBASE = get_peb_imagebase(PEB);
	unsigned long long PEB_LDR = get_peb_ldr(PEB);
	//bool isDebugged = is_being_debugged(PEB);

	// Retrieve the Kernel32 base address by traversing the InMemoryOrderModuleList LDR DoublyLinkedList
	unsigned long long DLL_BASE_KERNEL32 = get_kernel32_hmodule(PEB_LDR);

	// Retrieve the GetProcAddress address by traversing the Kernel32 MZ->PE->PE_OPTIONALS->DATA_DIRECTORY->EXPORT_DIRECTORY->EAT
	unsigned long long get_proc_address = get_getprocaddress_address(DLL_BASE_KERNEL32);
	typedef unsigned long long(*get_proc_address_t)(unsigned long long hModule, const char* lpProcName);
	get_proc_address_t f_get_proc_address = (get_proc_address_t)(get_proc_address);

	// Setup our LoadLibraryA function via GetProcAddress
	unsigned long long load_library = f_get_proc_address(DLL_BASE_KERNEL32, "LoadLibraryA");
	typedef unsigned long long(*load_library_t)(const char* lpFileName);
	load_library_t f_load_library = (load_library_t)(load_library);

	// Setup our FreeLibrary function via GetProcAddress
	unsigned long long free_library = f_get_proc_address(DLL_BASE_KERNEL32, "FreeLibrary");
	typedef unsigned long long(*free_library_t)(unsigned long long hModule);
	free_library_t f_free_library = (free_library_t)(free_library);

	// Load User32 module
	// DLL_BASE_USER32 = hModule
	unsigned long long DLL_BASE_USER32 = f_load_library("User32.dll");

	// Show MessageBoxA
	unsigned long long messageboxa = f_get_proc_address(DLL_BASE_USER32, "MessageBoxA");
	typedef int(*messageboxa_t)(unsigned long long hWnd, const char* lpText, const char* lpCaption, unsigned int uType);
	messageboxa_t f_messageboxa = (messageboxa_t)(messageboxa);

	f_messageboxa(0, "Hello World!", "It was a journey through PE!", 0);

	// Cleanup
	f_free_library(DLL_BASE_USER32);

	return 0;

	//x64_sh_get_kernel32();
}

/*
// The purpose of the following assembly block is to
// 1. Prepare the variables we are going to use
// 2. Load the PEB from the OS-given FS register (fs:ecx + 0x30)
// 3. Load the LDR to get the InMemoryOrderModuleList (which supplies us with a top-down chain of the most important kernel libraries)
// (TEB)->PEB->LDR->InMemoryOrderModuleList-> ProgramModule->NtDllModule->Kernel32Module
// 4. Traverse the InMemoryOrderModuleList until we have the Kernel32 base address in EBX for further usage
__declspec(noinline) static int x64_sh_get_kernel32() {
	__asm {
		xor rcx, rcx			// zeroing register ECX
		mul rcx					// zeroing register EAX EDX
		mov rax, GS:[0x60]		// PEB loaded in EAX		(EXCERPT #1, 2)
		mov rax, [rax + 0x018]	// LDR loaded in EAX		(EXCERPT #3, PPEB_LDR_DATA Ldr)
		mov rsi, [rax + 0x020]	// InMemoryOrderModuleList loaded in ESI (EXCERPT #3, LIST_ENTRY InMemoryOrderModuleList)
		lodsq					// program.exe address loaded in EAX(1st module)	(Load doubleword at address DS:ESI into EAX)
		xchg rsi, rax			// Exchange register data (swap)
		lodsq					// ntdll.dll address loaded(2nd module)
		mov rbx, [rax + 0x30]	// kernel32.dll address loaded in EBX(3rd module)

		// RBX = base of kernel32.dll address
	}
}*/
/*
static void x86_sh_get_kernel32() {
	__asm {
		xor ecx, ecx			// zeroing register ECX
		mul ecx					// zeroing register EAX EDX
		mov eax, FS: [0x30]		// PEB loaded in EAX		(EXCERPT #1, 2)
		mov eax, [eax + 0x00c]	// LDR loaded in EAX		(EXCERPT #3, PPEB_LDR_DATA Ldr)
		mov esi, [eax + 0x014]	// InMemoryOrderModuleList loaded in ESI (EXCERPT #3, LIST_ENTRY InMemoryOrderModuleList)
		lodsd					// program.exe address loaded in EAX(1st module)	(Load doubleword at address DS:ESI into EAX)
		xchg esi, eax			// Exchange register data (swap)
		lodsd					// ntdll.dll address loaded(2nd module)
		mov ebx, [eax + 0x10]	// kernel32.dll address loaded in EBX(3rd module)

		// EBX = base of kernel32.dll address
	}
}
*/
/*
// The purpose of this code is to retrieve the address of the "name" field
// from the PE file's data directory.
void sh_get_address_of_name() {
	__asm {
		mov edx, [ebx + 0x3c]	// Load e_lfanew address offset into EDX
		add edx, ebx			// Add EBX (base address of the PE file) to EDX
		// EDX = PE file base address + e_lfanew offset

		mov edx, [edx + 0x78]	// Load data directory address
		// EDX = address of data directory

		add edx, ebx			// Add EBX to EDX
		// EDX = base address + data directory

		mov esi, [edx + 0x20]	// Load "address of name"
		// ESI = address of "name" field

		add esi, ebx			// Add EBX to ESI		
		// ESI = base address + "name" field

		xor ecx, ecx			// Clear ECX
		// ESI = holds the address of the "name" field
	}
}

void sh_getprocaddress() {
	__asm {
		inc ecx	// ordinals increment
		lodsd// get "address of name" in eax
		add eax, ebx
		cmp dword ptr[eax], 0x50746547			// GetP
		jnz sh_getprocaddress
		cmp dword ptr[eax + 0x4], 0x41636F72	// rocA
		jnz sh_getprocaddress
		cmp dword ptr[eax + 0x8], 0x65726464	// ddre	(GetProcAddress)
		jnz sh_getprocaddress
	}
}
*/
// Excerpt #1:
// On x86 32bit windows the FS segment register points to a structure called the Thread Information Block 
// or the TIB for short.
// 
// This structure is created by the kernel on thread creation and is used to support OS related functionalities, services and APIs.

// Excerpt #2:
// This is the Process Environment Block
// https://processhacker.sourceforge.io/doc/ntpebteb_8h_source.html
// https://en.wikipedia.org/wiki/Process_Environment_Block

// Excerpt #3:
// The LDR Data contains information about the currently loaded modules for this process
// https://learn.microsoft.com/de-de/windows/win32/api/winternl/ns-winternl-peb_ldr_data

// Excerpt #4:
// e_lfanew this offset field tells Windows where to look for the file's PE header.
// https://www.nirsoft.net/kernel_struct/vista/IMAGE_DOS_HEADER.html

// Excerpt #5:
// 
// https://learn.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-image_nt_headers64
// https://learn.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-image_optional_header32